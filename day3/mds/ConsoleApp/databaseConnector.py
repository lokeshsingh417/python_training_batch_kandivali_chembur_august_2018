import pymysql as sql

def getDBConnection(ip="localhost",uname="root",password="",dbname = "mds"):
	try:
		db=sql.connect(ip,uname,password,dbname)
		cursor=db.cursor()
	except Exception as e:
		print("something went wrong!!!")
		raise e
	else:
		print("db connected successfully!!")
		return(db,cursor)

