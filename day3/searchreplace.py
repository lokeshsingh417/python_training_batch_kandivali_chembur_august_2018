print("Welcome to XYZ search&replace system".center(100,"*"))
paragraph = ""
recent_search = []
recent_replace = {}
paragraph = input("Enter your paragraph")
while (True):
	choice=int(input("1.Paragraph 2.Search 3.Replace 4.Recent Search 5.Recent Replace 6.Exit"))
	if(choice ==1):
		print(f"Your Paragraph :- {paragraph}".center(100,"*"))
	elif(choice ==2):
		search_word=input("Enter word to be Search!")
		search_index=paragraph.find(search_word)
		if(search_index<0):
			print(f"Your {search_word} is not present!!")
		else:
			print(f"Your {search_word} is present at index {search_index}!!")
		recent_search.append(search_word)		
	elif(choice ==3):
		old_word=input("Enter word to be Replace!")
		new_word=input("Enter word to be Replaced with!")
		paragraph=paragraph.replace(old_word,new_word)
		print(f"your new paragraph is :- {paragraph}".center(100,"*"))
		recent_replace[old_word]=new_word
	elif(choice ==4):
		print(f"your recent search history :- {recent_search[::-1]}".center(100,"*"))
	elif(choice==5):
		for key,value in list(recent_replace.items()):
			print(f"you have replace {key} with {value}".center(100,"*"))
	elif(choice ==6):
		print("Thanks for using Us".center(100,"*"))
		break
	else:
		print("Invalid Choice Please Select again!!!")		


