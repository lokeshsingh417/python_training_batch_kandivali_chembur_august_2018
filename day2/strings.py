# String
# CRUD operation 
# Function and methods 

# CREATE
a = "hello world"
print(a)

# READ
# positive direction
#[st:stop:+ve jump]
# st<stop

# negative direction
#[st:stop:-ve jump]
#st>stop

# first section
# print(a[1:5])
# print(a[1:7:2])
# print(a[1::2])
# print(a[:7:2])
# print(a[::3])
# print(a[::])

# Second Section
print(a[5:1:-1])
print(a[::-1])
print(a[9::-2])
print(a[:3:-1])
print(a[len(a)-5:len(a)])
print(a[::-2])

#Third Section 
print(a[-9:-2])
print(a[-9:9])
print(a[-2::-2])
print(a[-2:1:-1])
print(a[-1:-16:-1])
###############################
# Update  is not possible
# string is immutable 
print(a[:6]+"python")
print(a)
# ###############################

# # Delete
del a
print(a)

#################################
# function and methods 


a = "hello world"
#################
print(a.upper())
print(a.lower())
print(a.swapcase())
print(a.isupper())
print(a.islower())
##################

print(a.center(20,"*"))
print(a.ljust(20,"*"))
print(a.rjust(20,"*"))

##################
print(a.startswith("h"))
print(a.startswith("hello"))
print(a.startswith("Hello"))

print(a.endswith("d"))
print(a.endswith("world"))
print(a.endswith("World"))
#################

print(a.find("World"))

# index() raise exception
# # if string is not found  
print(a.index("World"))

# replace(old,new,occurance)
print(a.replace("world","python"))
print(a.replace("l",'z',2))

print(a.count("l"))

print(len(a))
########################

a =" hello world "
print(a.strip())
print(a.lstrip())
print(a.rstrip())


#####################
# a = "helloworld"
# a.replace("world"," world")


data=a.split(" ")
sep = " "
print(data)
print(sep.join(data))

a = " "
print(a.isspace())
a = "1234abc"
print(a.isdigit())
print(a.isalnum())

####################







###################





















