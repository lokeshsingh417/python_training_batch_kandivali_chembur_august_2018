# CRUD 
# CREATE READ UPDATE DELETE
# functions and method 
CREATE
a = 23 #int
b = 23.45 #float
c = 23+45j #complex

# #read
print(a,b,c)

# #number are immutable
# #number doesnt follow index property

# #update is not possible 
#a[1]=34
a = 34
print(a)

# delete
# del will flush out memory
# delete variable
del a,b,c 
print(a,b,c)
## functions and methods

a = -23.45
b = -25

print(abs(a))
print(abs(b))

import math as m
print(m.fabs(a))
print(m.fabs(b))
a = 23.3456
b = 23.57
c = 44.89
##################
print(m.ceil(a))
print(m.floor(a))
print(m.ceil(b))
print(round(a))
print(round(b))
print(round(a,1))
print(round(a,3))
#########
# cmp(a,b)
###########
print(m.log(a))
print(m.log10(b))
print(m.exp(c))
##################

# a = 25
# b = 35
# c = 45
# print(max(a,b,c))
# print(min(a,b,c))

# print(len(a))
########################
a = 23.456
b = 23.568
print(m.modf(a))
print(m.modf(b))
#########################
#Random Package

import random as r
###############
print(r.choice("hello world"))
 print(r.random()) #  0>num>1
# ########
print(r.choice(list(range(10000,1000000))))
# ########
size = 5
captha=[]
for i in range(size):
	captha.append(r.choice("hello world"))
print("".join(captha))
########

#print(r.uniform(20,50))

print(r.randrange(200,100,-1))

# list1 = [x for x in range(1,10)]
# list1 = list(range(1,10))
# print(r.shuffle(list1))
# print(list1)

#r.seed(10)
#print(r.random())
######################################












































