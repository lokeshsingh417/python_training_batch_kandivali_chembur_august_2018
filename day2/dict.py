# CRUD On Dictionary
#CREATE
# dict1 = {
# 	"name":"lokesh",
# 	"std":"BE",
# 	"roll_no":36,
# 	"courses":["java","python"],
# 	"address":{"landmark":"lalbaug",
# 				"pincode":"400033"},

# }
# # print(dict1)

# ## READ
# # print(dict1["std"])

# ## UPDATE
# # dict1["name"]="raj"
# # print(dict1)

# ## DELETE
# del dict1["name"]
# print(dict1)

## Functions and methods
# dict1.clear()
# print(dict1)

dict1 = {"name":"lokesh",
		"std":"BE",
		"roll_no":36}
# dict2 = dict1
# dict3 = dict1.copy()
# # print(dict1)
# # print(dict2)
# # print(dict3)
# dict1["name"]="rajesh"
# print(dict1)
# print(dict2)
# print(dict3)
# usuage of copy()

########
# list1 = ["name","std","roll_no","address"]
# print({}.fromkeys(list1,"NA"))
########
#print(dict1.__contains__("name"))
########
#print(dict1.get("name"))
########	
# print(dict1.setdefault("address"))
# print(dict1)
########
# dict2 = {"contact_no":"8879970286",
# 		"address":"lalbaug"}
# dict1.update(dict2)
# print(dict1)
########
# print(list(dict1.keys()))
# print(list(dict1.values()))
# print(list(dict1.items()))
########









