# Datatype 
# number system 
# a = 23
# b = 23.45
# c = 23+32j
# #type(variable_name)-->
# #datatype of variable
# print(type(a),a)
# print(type(b),b)
# print(type(c),c)
#####################
# a = "hello world"
# b = 'hello all'
# c = ''' Hey All'''
# d = """ Check This!!!"""
# print(a,b,c,d)
######################
# # List 
# a = [1,2,3,4,5]
# b = ["a","b","c"]
# c = [1,2,"hey"]
# d = [1,23.45,23+34j]
# e = [[1,2,3],["a","b"]]
# print(a,b,c,d,e)
# #print(a)
# # print(b)
# # print(c)
# # print(d)
# # print(e)
######################

# Tuples
# a = (1,2,3)
# b = ("a","b")
# c = (1,"a")
# d = (1,23.45,23+45j)
# e = ((1,2),("a","b"))
# print(a,b,c,d,e)
#####################
# # Dictionary
# student_dict={"name":
# "lokesh","std":"BE",
# "roll_no":36,
# "courses":["java","python"]
# }
# print(student_dict)
#######################

# a = {1,2,3,4,5}
# b = {1,3,4,5,6,7}
# print(a.union(b))
# print(a-b)
# print(b-a)
#######################
















