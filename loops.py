# loop 
# while else

# counter =0
# while(counter<10):
# 	print(counter)
# 	counter+=1
# else:
# 	print("good by")
	

# for loop 
# a = "hello world"
# for i in a:
# 	print(i)
# print(list(enumerate("hello world",4)))
# #enumerate will give the iteration index
# for index,data in enumerate("hello world"):
# 	print(data,index)

# a = ["red","green","blue"]
# b = ["rose","vegetable","sky"]
# c = [1,2,3]

# for index,data in enumerate(zip(a,b,c)):
# 	print(data[0],data[1],data[2])


####
# for row in range(1,101):
# 	for column in range(1,11):
# 		print(row*column,end=" ")
# 	print()	
###########################
#control statement

# break

# for i in "hello world":
# 	if(i=="w"):
# 		break
# 		print("good by")
# 	print(i)


# for i in "hello world":
# 	if(i=="w"):
# 		continue
# 		print("good by")
# 	print(i)


# for i in "hello world":
# 	if(i=="w"):
# 		pass
# 		break
# 	print(i)
##########################################


while(1==1):
	print("hello")



